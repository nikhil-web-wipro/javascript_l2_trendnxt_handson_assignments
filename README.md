# TrendNxt HandsOn Assignments for JavaScript L2

## Every assignment is inside the respective folder with file name inde.html

# Topic 1 : JavaScript Strings
    Assignment 1 
    Assignment 2 
    Assignment 3

# Topic 2 : JavaScript Numbers
    Assignment 1

# Topic 3 : JavaScript Math
    Assignment 2
    Assignment 3

# Topic 4 : JavaScript Date
    Assignment 1
    Assignment 2

# Topic 5 : JavaScript Arrays
    Assignment 1
    Assignment 3

# Topic 6 : JavaScript Booleans And Type Of
    Assignment 2

# Topic 7 : JavaScript Type Conversion
    Assignment 1

# Topic 8 : JavaScript Regular expression
    Assignment 2

# Topic 9 : JavaScript form validation
    Assignment 1

# Topic 10 : Javascript HTML DOM
    Assignment 1
    Assignment 2

# Topic 11 : Javascript Browser BOM
    Assignment 1
    Assignment 2